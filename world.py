import os

import pygame


class Object(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height, name=None):
        super().__init__()
        self.rect = pygame.Rect(x, y, width, height)
        self.image = pygame.Surface((width, height), pygame.SRCALPHA)
        self.width = width
        self.height = height
        self.name = name

    def draw(self, win, offset_x):
        win.blit(self.image, (self.rect.x - offset_x, self.rect.y))


class Block(Object):
    def __init__(self, x, y, size):
        super().__init__(x, y, size, size)
        block = load_block(size, "Grass.png")
        self.image.blit(block, (0, 0))
        self.mask = pygame.mask.from_surface(self.image)


class Tube(Object):
    def __init__(self, x, y, size):
        super().__init__(x, y, size, size)
        block = load_block(size, "Tube.png")
        self.image.blit(block, (0, 0))
        self.mask = pygame.mask.from_surface(self.image)


class Brick(Object):
    def __init__(self, x, y, size):
        super().__init__(x, y, size, size)
        block = load_block(size, "Brick.png")
        self.image.blit(block, (0, 0))
        self.mask = pygame.mask.from_surface(self.image)


class Cloud(Object):
    def __init__(self, x, y, size):
        super().__init__(x, y, size, size)
        cloud = load_block(size, "Cloud.png")
        self.image.blit(cloud, (0, 0))
        self.mask = pygame.mask.from_surface(self.image)


class QuestionMark(Object):
    def __init__(self, x, y, size):
        super().__init__(x, y, size, size)
        block = load_block(size, "Question.png")
        self.image.blit(block, (0, 0))
        self.mask = pygame.mask.from_surface(self.image)


def load_block(size, block_name):
    block_path = os.path.join("assets", "World", block_name)
    image = pygame.image.load(block_path).convert_alpha()
    surface = pygame.Surface((size, size), pygame.SRCALPHA, 32)
    surface.blit(image, (0, 0))
    return surface

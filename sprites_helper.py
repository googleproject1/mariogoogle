import pygame
import os

COLOR_DEPTH = 32


def flip(sprites):
    return [pygame.transform.flip(sprite, True, False) for sprite in sprites]


def load_sprite_sheets(sprites_directory, sprite_width, sprite_height, direction=False):
    path = os.path.join("assets", sprites_directory)
    images = [sprite_file for sprite_file in os.listdir(path) if os.path.isfile(os.path.join(path, sprite_file))]

    all_sprites = {}

    for image_filename in images:
        sprite_sheet = pygame.image.load(os.path.join(path, image_filename)).convert_alpha()

        sprites = []
        for sprite_number in range(sprite_sheet.get_width() // sprite_width):
            surface = pygame.Surface((sprite_width, sprite_height), pygame.SRCALPHA, COLOR_DEPTH)
            rect = pygame.Rect(sprite_number * sprite_width, 0, sprite_width, sprite_height)
            surface.blit(sprite_sheet, (0, 0), rect)
            sprites.append(surface)

        if direction:
            all_sprites[image_filename.replace(".png", "") + "_right"] = sprites
            all_sprites[image_filename.replace(".png", "") + "_left"] = flip(sprites)
        else:
            all_sprites[image_filename.replace(".png", "")] = sprites
    return all_sprites

import pygame


def handle_vertical_collision(player, objects, y_velocity):
    collided_objects = []
    for obj in objects:
        if pygame.sprite.collide_mask(player, obj):
            if y_velocity > 0:
                player.rect.bottom = obj.rect.top
                player.landed()
            elif y_velocity < 0:
                player.rect.top = obj.rect.bottom
        collided_objects.append(obj)
    return collided_objects


def horizontal_collision(player, objects, dx):
    player.move_rect(dx, 0)
    player.update()
    collided_object = None
    for obj in objects:
        if pygame.sprite.collide_mask(player, obj):
            collided_object = obj
            break

    player.move(-dx, 0)
    player.update()
    return collided_object

import random

import pygame
from world import Block, Tube, QuestionMark, Brick, Cloud
from collision import handle_vertical_collision
from sprites_helper import load_sprite_sheets

FPS = 60
PLAYER_VEL = 5
SCREEN_WIDTH = 811
SCREEN_HEIGHT = 508
BLOCK_WIDTH = 50
BLOCK_HEIGHT = 50
TUBE_SIZE = 80
BLOCK_SIZE = 80
CLOUD_SIZE = 100
SKY_COLOR = (135, 206, 235)
BLOCKS = [QuestionMark, Brick]
pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Super Mario')


class Player(pygame.sprite.Sprite):
    COLOR = (255, 0, 0)
    GRAVITY = 1
    SPRITES = load_sprite_sheets("Mario", 50, 75, True)
    # SPRITES = load_sprite_sheets("Mario", 32, 32, True)
    # SPRITES = load_sprite_sheets("Mario", 50,50 , True)
    ANIMATION_DELAY = 2

    def __init__(self, x, y, width, height):
        super().__init__()
        self.rect = pygame.Rect(x, y, width, height)

        # How fast per fame the player moves
        self.x_velocity = 0
        self.y_velocity = 0
        self.mask = None
        self.animation_direction = "left"
        self.animation_count = 0
        self.fall_count = 0
        self.jump_count = 0

    def move_rect(self, dx, dy):
        self.rect.x += dx
        self.rect.y += dy

    def move_left(self, vel):
        self.x_velocity = -vel
        if self.animation_direction != "left":
            self.animation_direction = "left"
            self.animation_count = 0

    def move_right(self, vel):
        self.x_velocity = vel
        if self.animation_direction != "right":
            self.animation_direction = "right"
            self.animation_count = 0

    def loop(self, fps):
        self.y_velocity += min(1, (self.fall_count / fps) * self.GRAVITY)
        self.move_rect(self.x_velocity, self.y_velocity)
        self.update_sprite()
        self.fall_count += 1

    def draw(self, win, offset_x):
        win.blit(self.sprite, (self.rect.x - offset_x, self.rect.y))

    def jump(self):
        self.y_velocity = -self.GRAVITY * 10
        self.animation_count = 0
        self.jump_count += 1
        if self.jump_count == 1:
            self.fall_count = 0

    def landed(self):
        self.fall_count = 0
        self.y_velocity = 0
        self.jump_count = 0

    def update_sprite(self):
        sprite_sheet = "idle"
        if self.x_velocity != 0:
            sprite_sheet = "walk"
        if self.y_velocity < 0:
            if self.jump_count == 1:
                sprite_sheet = "jump"

        sprite_sheet_name = sprite_sheet + "_" + self.animation_direction
        sprites = self.SPRITES[sprite_sheet_name]
        sprite_index = (self.animation_count //
                        self.ANIMATION_DELAY) % len(sprites)
        self.sprite = sprites[sprite_index]
        self.animation_count += 1
        self.update()

    def update(self):
        self.rect = self.sprite.get_rect(topleft=(self.rect.x, self.rect.y))
        self.mask = pygame.mask.from_surface(self.sprite)


# load images
player = Player(0, 50, 50, 75)


def handle_player_movement(player, objects):
    keys = pygame.key.get_pressed()
    player.x_velocity = 0
    if keys[pygame.K_LEFT]:
        player.move_left(PLAYER_VEL)
    if keys[pygame.K_RIGHT]:
        player.move_right(PLAYER_VEL)
    handle_vertical_collision(player, objects, player.y_velocity)


def generate_ground():
    blocks = []
    for block_x in range(0, SCREEN_WIDTH // BLOCK_WIDTH + 1):
        blocks.append(Block(block_x * BLOCK_WIDTH, SCREEN_HEIGHT - BLOCK_HEIGHT, BLOCK_WIDTH))
    return blocks


def generate_tube():
    return Tube(SCREEN_WIDTH // 2 + random.randint(50, 200), SCREEN_HEIGHT - BLOCK_HEIGHT - TUBE_SIZE, TUBE_SIZE)


def genreate_clouds():
    clouds = []
    x_position = random.randint(70, 120)
    for cloud_number in range(0, 3):
        y_position = random.randint(0, 10)
        clouds.append(Cloud(x_position + cloud_number * CLOUD_SIZE, y_position, CLOUD_SIZE))
        x_position = x_position + cloud_number * CLOUD_SIZE + random.randint(400, 500)
    return clouds


def generate_bricks():
    bricks = []
    x_position = random.randint(70, 120)
    y_position = SCREEN_HEIGHT // 2 - random.randint(0, 50)
    for series in range(0, 2):
        number_of_bricks = random.randint(1, 4)
        for brick_number in range(0, number_of_bricks):
            bricks.append(random.choice(BLOCKS)(x_position + brick_number * BLOCK_SIZE, y_position, BLOCK_SIZE))
        x_position = x_position + number_of_bricks * BLOCK_SIZE + random.randint(200, 400)
    return bricks


def draw(screen, player, objects):
    # Fill background
    screen.fill(SKY_COLOR)
    for object in objects:
        object.draw(screen, 0)

    player.draw(screen, 0)


def main():
    blocks = generate_ground()
    blocks.append(generate_tube())
    blocks.extend(generate_bricks())
    blocks.extend(genreate_clouds())
    # define game variables
    run = True
    while run:
        clock.tick(FPS)

        player.loop(FPS)
        handle_player_movement(player, blocks)
        draw(screen, player, blocks)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                break
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and player.jump_count < 2:
                    player.jump()
        pygame.display.update()

    pygame.quit()


if __name__ == "__main__":
    main()
